document.write("<h1>Auxi apruebeme</h1>");

/* TIPOS DE DATOS */
"hello world" /* --> cadena=string */
'hello world' /* --> cadena con comillas simples */

1, 2, 3, 4, 5, 6, 7, 8, 9, 10  /* --> enteros = int */
0.1, 0.2, 0.3, 0.4, 0.5  /* --> float */

/* true, false  -->  Boolean */

/* array = arreglos */
['joe', 'jack', 'adil', 'jack'];  /* --> lista */
[1, 2, 3, 4, 5, 6, 7, 8, 9];     /* --> lista de numeros */

/* object = objetos */

'ryan'
70.4
14
true

{
    username: 'ryan';
    score: 70.4;
    hours: 14;
    proffesional: true;
}

{
username: 'joe';
score: 70;
hours: 10;
    proffesional: false;
} 

console.log("esto es un cadena");

console.log(1, 2, 3, 4);

console.log({"username":'ryan', "score": 14});

/* //VARIABLES     --> pueden variar en la memoria */

var nombredelusuario = "john";
console.log(nombredelusuario);

let lastname = "carter";
console.log(lastname);

nombredelusuario = 'pepe';
console.log(nombredelusuario);

const pi = 3.1415;      /* -->     no se puede cambiar el valor */
console.log(pi);


let numerouno = 60;
let numerodos = 100;

let suma = numerouno + numerodos;
let resta = numerouno - numerodos;
let multi = numerouno * numerodos;
let divi = numerouno / numerodos;
console.log(suma);
console.log(resta);
console.log(multi);
console.log(divi);

let nombre = "john";
let apellido = "carter";

let completo = nombre + ' ' + apellido;
console.log(completo);

/* CONDICIONALES */


let numeroalto = 500;
let numerobajo = 100;


let igual = numeroalto == numerobajo;
let mayor = numeroalto > numerobajo;   
let menor = numeroalto < numerobajo;   
let diferente = numeroalto != numerobajo;   
console.log(igual);
console.log(mayor);
console.log(menor);
console.log(diferente);

let passwordBD = 'pepe123';

let input = 'pepe123';

let resultado = input == passwordBD;
console.log(resultado);

console.log("----------------------------------");
/* CONTROL DE FLUJO */
/* CONDICIONALES */

if(resultado === true) {        /* usar tiple = para comparar mayusculas  */
    console.log("login correcto");
}else{
    console.log("login incorrecto");
}

let punto = 70;

if (punto > 50) {
    console.log("sabes jugar");
} 
else if (punto > 30) {
    console.log("estas mejorando");
}
else {
    console.log("ve este tutorial")
}

console.log("----------------------------------");

let tipotarjeta  = "credito";

switch (tipotarjeta) {
    case "debito":
        console.log("tarjeta de debito");
        break;
    case "credito":
        console.log("tarjeta de credito");
        break;
    default:    console.log("no se encuentra");
        break;
}

console.log("----------------------------------");

let cantidad = 30;
let int = 1;
while (cantidad > 0) {
    console.log("hola usuario"+ ' ' + int)
    cantidad--;
    int++;
}

console.log("-------------------------------------");

/* BUCLES */

let nombres = ['ryan', 'John', 'jack', 'Bob', 'aisl'];

for (let i = 0; i < nombres.length; i++) {
    console.log(nombres[i]);
    
}

let numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9];

for (let i = 0; i < numeros.length; i++) {
    console.log(numeros[i]);
    
}

function saludar() {
    console.log("hello world function");
    
}

saludar();


function hola(nombredepersona) {
    console.log(nombredepersona + " " + "hello world");
    console.log("hello world")
}

hola('ryan');

function add(n1, n2) {
    console.log(n1+n2)
    
}

add(3, 2);
add(100, 300);
add('asdasd', 30);